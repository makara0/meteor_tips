import './header.html';

import Swal from 'sweetalert2';
const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
})
Template._header.helpers({
    active: function(path) {
        if (Router.current().route !== undefined) {
            var routename = Router.current().route.getName();
            var rsArray = path.split(',');
            var rsCheck = rsArray.includes(routename);
            if (rsCheck) {
                return "active";
            }
        }
    },
    checkName: () => {
        return Router.current().route.getName();
    }
})

Template._header.events({
    'click .animation': (event) => {
        window.scroll({
            top: 0,
            behavior: 'smooth'
        });
        // $("html, body").animate({ scrollTop:0 }, 500);
    },
    'keyup #text-message': (event) => {
        if (event.target.value !== '') {
            $('.btn-send-comment').removeClass('disabled');
        } else {
            $('.btn-send-comment').addClass('disabled');
        }

        let value = $('#text-message').attr('rows');
        if (event.keyCode === 13) {
            $('#text-message').attr('rows', parseFloat(value) + 1);
        }
    },
    'click .btn-send-comment': (event) => {
        let value = $('#text-message').val();
        if (value !== '') {
            Meteor.call('CommentMethod', value, Router.current().params.id, (err, rs) => {
                if (rs !== '' && rs !== undefined) {
                    $('#text-message').val('');
                    $('#text-message').attr('rows', 1);
                    Toast.fire({
                        icon: 'success',
                        title: 'Comment Successfully.'
                    })
                }
            })
        }
        $('.hidden-by-focus').removeClass('hidden');
        $('.comment-bar').addClass('class-for-check');
    },
    'focus #text-message': (event) => {
        $('.hidden-by-focus').addClass('hidden');
        $('.comment-bar').removeClass('class-for-check');
    },
    'focusout #text-message': (event) => {
        $('.hidden-by-focus').removeClass('hidden');
        $('.comment-bar').addClass('class-for-check');
    },
})
