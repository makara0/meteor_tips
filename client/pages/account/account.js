import { Template } from 'meteor/templating';
import './account.html';
Template.myaccount.helpers({
    accountstate: function() {
        var state = AccountsTemplates.getState();
        return AccountsTemplates.getState();
    }
})
Template.myaccount.events({
    'click #shop': function() {
        $("#at-field-role").val($("input[name='urole']:checked").val());
        $('#at-btn').prop("type", "submit");
        localStorage.setItem("mkit_role_select", $("input[name='urole']:checked").val());
    },
    'click #customer': function() {
        $("#at-field-role").val($("input[name='urole']:checked").val());
        $('#at-btn').prop("type", "submit");
        localStorage.setItem("mkit_role_select", $("input[name='urole']:checked").val());
    },
})

Accounts.onLogin(function() {
    var checkrole = Roles.userIsInRole(Meteor.userId(), ['admin', 'shop', 'customer']);
    if (checkrole === false) {
        Meteor.call("setRoleForExternal", localStorage.getItem("mkit_role_select")); //user
        localStorage.removeItem("mkit_role _select");
    }
});
