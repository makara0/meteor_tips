// Main Layout
import './layouts/mainLayout';

// components
import './components/loading/loading';
import './components/header/header';
import './components/footer/footer';

// pages
import './pages/auth/auth';
import './pages/home/home';
import './pages/account/account';
import './pages/profile/profile';
import './pages/photo-profile/photo_profile';
import './pages/list-user/user';

// pages Admin
import './admin/member/list/list';
import './admin/province/record/record';