import { Partners } from "../libs/collections/partner";
import Swal from 'sweetalert2';
import Util from './util';
import '/client/register';
Router.configure({
    layoutTemplate: 'MainLayout',
    notFoundTemplate: 'notFound',
    loadingTemplate: 'loading',
    waitOn() {
      Session.set('loadingSplash', false);
    },
    subscriptions() {
      return Meteor.subscribe('signedinPartner');
    },
    onRun() {
      if ( Meteor.isClient ) {
        if (Session.get('isAlertFillProfile')) {
            Swal.fire({
                title:  "Confirmation",
                text: "You must be field information",
                icon: 'info',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Ok"
              }).then((result) => {
                if (result.isConfirmed) {
                    Session.set('isAlertFillProfile', false);
                    Router.go("/profile");
                }
            })
        }
      }
      this.next();
    },
    onBeforeAction() {
        checkProfileInfo();
        this.next();
    }
});

Router.plugin('ensureSignedIn', {
    except: ['home', 'atSignIn', 'atSignUp', 'atForgotPassword']
});

Router.route('/logout', function() {
    Meteor.logout(function(err) {
        if (!err) Router.go('/');
    });
});

Router.route('/', {
    name: 'home',
    subscriptions() {
      return Meteor.subscribe('signedinPartner');
    }
});

Router.route('/profile', function() {
    this.render('profile');
}, { name: 'profile' });

Router.route('/show/user', function() {
    this.render('DataUser');
}, { name: 'test_list' });

//===================================
// Admin Router
//===================================
Router.route('/admin/member/list', function() {
    this.render('admin_member_list');
}, { name: 'admin_member_list' });

Router.route('/admin/province/record', function() {
    this.render('province_record');
}, { name: 'province_record' });

//===================================
// End Admin Router
//===================================

//check User Profile
function checkProfileInfo() {
    let name = Router.current().route.getName();
    let routes = ['public'];
    if (routes.indexOf(name) === -1) 
    {
        if(Roles.userIsInRole(Meteor.user(), ['employee', 'employer'])){
            let profile = Partners.findOne({account: Meteor.userId()});
            // check user blocked
            if (profile && profile.blocked) {
                Router.go('/terminated');
            }else if (!profile || !profile.isCompleted) {
                if (!profile && Session.get('isAlertFillProfile') === undefined) {
                    Session.set('isAlertFillProfile', true);
                }
                Router.go('/profile');
            }
        }
    }
}