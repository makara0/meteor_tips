import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
export const Province = new Mongo.Collection('Province');

const Schemas = {};

Schemas.Province = new SimpleSchema({
    name: {
        type: String,
    },
    view_order: {
        type: Number,
    },
    createdAt: {
        type: Date,
        label: 'Date',
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    }
}, { tracker: Tracker });

Province.attachSchema(Schemas.Province);

if (Meteor.isServer) 
{
    Province.allow({
        insert: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        update: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        remove: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        }
    });

    Province.after.insert(function (userId, doc) {
        
    });
    
    Province.before.insert(function (userId, doc) {
        
    });
}