Meteor.startup(function() {
    //Add defaul user roles
    var roles = ['admin', 'employee', 'employer'];
    // this will fail if the roles package isn't installed
    if (Meteor.roles.find().count() === 0) {
        roles.map(function(role) {
            Roles.createRole(role)
        })
    };
});
Meteor.methods({
    setRoleForExternal: function(role) {
        var checkrole = Roles.userIsInRole(this.userId, ['admin', 'shop', 'customer']);
        if (checkrole === false) {
            if (role === "shop") {
                Roles.setUserRoles(this.userId, 'shop');
            } else if (role == 'customer') {
                Roles.setUserRoles(this.userId, 'customer');
            }
        }
    }
})
